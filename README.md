Создание рейда

Зануляем суперблоки
[vagrant@otuslinux ~]$ mdadm --zero-superblock --force /dev/sd{b,c,d,e,f,g,h,i}
 
Создаем рейд 10 из 8 дисков
[vagrant@otuslinux ~]$ mdadm --create --verbose /dev/md0 -l 10 -n 8 /dev/sd{b,c,d,e,f,g,h,i} 

Проверяем, что рейд собрался
[vagrant@otuslinux ~]$ sudo mdadm --detail /dev/md0
/dev/md0:
           Version : 1.2
     Creation Time : Sun Sep 19 21:38:22 2021
        Raid Level : raid10
        Array Size : 1015808 (992.00 MiB 1040.19 MB)
     Used Dev Size : 253952 (248.00 MiB 260.05 MB)
      Raid Devices : 8
     Total Devices : 8
       Persistence : Superblock is persistent

       Update Time : Sun Sep 19 21:38:28 2021
             State : clean
    Active Devices : 8
   Working Devices : 8
    Failed Devices : 0
     Spare Devices : 0

            Layout : near=2
        Chunk Size : 512K

Consistency Policy : resync

              Name : otuslinux:0  (local to host otuslinux)
              UUID : 236b3218:33360669:e1580991:d3bd76ba
            Events : 19

    Number   Major   Minor   RaidDevice State
       0       8       16        0      active sync set-A   /dev/sdb
       1       8       32        1      active sync set-B   /dev/sdc
       2       8       48        2      active sync set-A   /dev/sdd
       3       8       64        3      active sync set-B   /dev/sde
       4       8       80        4      active sync set-A   /dev/sdf
       5       8       96        5      active sync set-B   /dev/sdg
       6       8      112        6      active sync set-A   /dev/sdh
       7       8      128        7      active sync set-B   /dev/sdi
